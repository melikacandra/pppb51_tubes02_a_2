package com.example.faesttle;

public class Static {
    //string page untuk pindah halaman
    public static final String PAGE = "page";
    public static final String CHANGE_PAGE = "changePage";
    //kode fragment
    public static final int MAIN_FRAGMENT = 1;
    public static final int LOGIN_FRAGMENT = 2;
    public static final int PESAN_FRAGMENT = 3;
    public static final int RUTE_FRAGMENT = 4;
    public static final int SHUTTLE_FRAGMENT = 5;
    public static final int HISTORY_FRAGMENT = 6;
    public static final int PEMBAYARAN_FRAGMENT = 7;
    public static final int SMALL_SEATS_FRAGMENT = 8;
    public static final int LARGE_SEATS_FRAGMENT = 9;
    public static final int EXIT_CLOSE = 100;

    //nama fragment di menu
    public static final String LOGIN = "Login";
    public static final String MAIN = "Home";
    public static final String HISTORY = "History";
    public static final String RUTE = "Lihat Rute";
    public static final String SHUTTLE = "Cari Shuttle";
    public static final String EXIT = "Keluar";

}
