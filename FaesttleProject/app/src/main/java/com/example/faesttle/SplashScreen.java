package com.example.faesttle;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.Nullable;

public class SplashScreen extends Activity {
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_screen);

        this.videoView= (VideoView) findViewById(R.id.videoview);
        Uri video = Uri.parse("android.resource://" +getPackageName() +"/"+R.raw.splashscreenvideo);
        this.videoView.setVideoURI(video);
        this.videoView.setMediaController(new MediaController(this));


        this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                startActivity(new Intent(SplashScreen.this,MainActivity.class));
                finish();
            }
        });

        this.videoView.start();
    }
}
