package com.example.faesttle;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class RutePresenter {
    protected List<Rute> rutes;
    protected IRuteFragment ui;

    public RutePresenter(IRuteFragment view){
        this.ui = view;
        this.rutes = new ArrayList<Rute>();
    }

    public void loadData(Context context) throws JSONException {
        this.getRoutes(context);
    }
    public void getRoutes(Context context) throws JSONException{

    }
    public void aksesAPI(){

    }

    public interface IRuteFragment{
        public void tampilData(List<Rute> rutes);
    }
}
