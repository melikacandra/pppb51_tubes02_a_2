package com.example.faesttle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    FragmentManager fragmentManager;
    MainFragment mainFragment;
    LoginFragment loginFragment;
    PesanFragment pesanFragment;
    RuteFragment ruteFragment;
    ShuttleFragment shuttleFragment;
    HistoryFragment historyFragment;
    PembayaranFragment pembayaranFragment;
    SmallSeatsFragment smallSeatsFragment;
    LargeSeatsFragment largeSeatsFragment;

    Toolbar toolbar;
    DrawerLayout drawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mainFragment = MainFragment.newInstance();
        this.loginFragment = LoginFragment.newInstance();
        this.pesanFragment = PesanFragment.newInstance();
        this.ruteFragment = RuteFragment.newInstance();
        this.shuttleFragment = ShuttleFragment.newInstance();
        this.historyFragment = HistoryFragment.newInstance();
        this.pembayaranFragment = PembayaranFragment.newInstance();
        this.smallSeatsFragment = SmallSeatsFragment.newInstance();
        this.largeSeatsFragment = LargeSeatsFragment.newInstance();

        this.fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction ft = this.fragmentManager.beginTransaction();

        ft.add(R.id.fragment_container,this.loginFragment)
                .addToBackStack(null)
                .commit();
        this.toolbar = findViewById(R.id.toolbar);
        this.drawer = findViewById(R.id.drawer_layout);
        this.setSupportActionBar(toolbar);

        ActionBarDrawerToggle abdt = new ActionBarDrawerToggle(this, drawer,toolbar,R.string.openDrawer, R.string.closeDrawer );
        drawer.addDrawerListener(abdt);
        abdt.syncState();

        //menerima dari klik left fragment
        this.getSupportFragmentManager().setFragmentResultListener(
                Static.CHANGE_PAGE, this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, Bundle result) {
                        int page = result.getInt(Static.PAGE);
                        Log.d(Static.CHANGE_PAGE, Integer.toString(page));
                        changePage(page);
                    }
                }
        );

    }
    public void changePage(int page) {
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        switch (page) {
            case Static.MAIN_FRAGMENT:
                if (this.mainFragment.isAdded()) {
                    ft.show(this.mainFragment);
                } else {
                    ft.add(R.id.fragment_container, this.mainFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.historyFragment.isAdded()) {
                    ft.hide(this.historyFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.LOGIN_FRAGMENT:
                if (this.loginFragment.isAdded()) {
                    ft.show(this.loginFragment);
                } else {
                    ft.add(R.id.fragment_container, this.loginFragment);
                }
                //fragment yang harus tertutup
                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }
                //fragment yang harus tertutup
                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.historyFragment.isAdded()) {
                    ft.hide(this.historyFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.PESAN_FRAGMENT:
                if (this.pesanFragment.isAdded()) {
                    ft.show(this.pesanFragment);
                } else {
                    ft.add(R.id.fragment_container, this.pesanFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.historyFragment.isAdded()) {
                    ft.hide(this.historyFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.RUTE_FRAGMENT:
                if (this.ruteFragment.isAdded()) {
                    ft.show(this.ruteFragment);
                } else {
                    ft.add(R.id.fragment_container, this.ruteFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.historyFragment.isAdded()) {
                    ft.hide(this.historyFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.SHUTTLE_FRAGMENT:
                if (this.shuttleFragment.isAdded()) {
                    ft.show(this.shuttleFragment);
                } else {
                    ft.add(R.id.fragment_container, this.shuttleFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }

                if (this.historyFragment.isAdded()) {
                    ft.hide(this.historyFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.HISTORY_FRAGMENT:
                if (this.historyFragment.isAdded()) {
                    ft.show(this.historyFragment);
                } else {
                    ft.add(R.id.fragment_container, this.historyFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                break;
            case Static.PEMBAYARAN_FRAGMENT:
                if (this.pembayaranFragment.isAdded()) {
                    ft.show(this.pembayaranFragment);
                } else {
                    ft.add(R.id.fragment_container, this.pembayaranFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                break;
            case Static.SMALL_SEATS_FRAGMENT:
                if (this.smallSeatsFragment.isAdded()) {
                    ft.show(this.smallSeatsFragment);
                } else {
                    ft.add(R.id.fragment_container, this.smallSeatsFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.largeSeatsFragment.isAdded()) {
                    ft.hide(this.largeSeatsFragment);
                }

                break;
            case Static.LARGE_SEATS_FRAGMENT:
                if (this.largeSeatsFragment.isAdded()) {
                    ft.show(this.largeSeatsFragment);
                } else {
                    ft.add(R.id.fragment_container, this.largeSeatsFragment);
                }
                //fragment yang harus tertutup
                if (this.loginFragment.isAdded()) {
                    ft.hide(this.loginFragment);
                }

                if (this.pesanFragment.isAdded()) {
                    ft.hide(this.pesanFragment);
                }

                if (this.ruteFragment.isAdded()) {
                    ft.hide(this.ruteFragment);
                }

                if (this.shuttleFragment.isAdded()) {
                    ft.hide(this.shuttleFragment);
                }

                if (this.mainFragment.isAdded()) {
                    ft.hide(this.mainFragment);
                }
                if (this.pembayaranFragment.isAdded()) {
                    ft.hide(this.pembayaranFragment);
                }
                if (this.smallSeatsFragment.isAdded()) {
                    ft.hide(this.smallSeatsFragment);
                }
                break;
            default:
                closeApplication();
                break;
        }
        ft.commit();
    }
    public void closeApplication(){
        this.moveTaskToBack(true);
        this.finish();
    }
}