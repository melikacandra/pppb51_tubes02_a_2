package com.example.faesttle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.faesttle.databinding.FragmentHistoryBinding;
import com.example.faesttle.databinding.FragmentPembayaranBinding;

public class PembayaranFragment extends Fragment {
    private FragmentPembayaranBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    public static PembayaranFragment newInstance(){
        PembayaranFragment fragment = new PembayaranFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentPembayaranBinding.inflate(inflater, container, false);

        return this.binding.getRoot();
    }
}
