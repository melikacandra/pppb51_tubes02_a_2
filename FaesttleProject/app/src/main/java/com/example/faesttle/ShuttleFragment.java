package com.example.faesttle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.faesttle.databinding.FragmentPesanBinding;
import com.example.faesttle.databinding.FragmentShuttleBinding;

public class ShuttleFragment extends Fragment {
    private FragmentShuttleBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    public static ShuttleFragment newInstance(){
        ShuttleFragment fragment = new ShuttleFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentShuttleBinding.inflate(inflater, container, false);

        return this.binding.getRoot();
    }
}
