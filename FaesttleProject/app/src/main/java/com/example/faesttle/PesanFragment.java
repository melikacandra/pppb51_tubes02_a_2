package com.example.faesttle;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.faesttle.databinding.FragmentLoginBinding;
import com.example.faesttle.databinding.FragmentPesanBinding;

public class PesanFragment extends Fragment implements View.OnClickListener {
    private FragmentPesanBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    public static PesanFragment newInstance(){
        PesanFragment fragment = new PesanFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentPesanBinding.inflate(inflater, container, false);
        this.binding.btnCari.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if(v == binding.btnCari){
            Bundle result = new Bundle();
            result.putInt(Static.PAGE, Static.SHUTTLE_FRAGMENT);
            this.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
        }
    }
}
