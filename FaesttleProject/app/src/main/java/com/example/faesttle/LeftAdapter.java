package com.example.faesttle;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

public class LeftAdapter extends BaseAdapter {
        private ArrayList<String> list;
        private Fragment fragment;
        private TextView tv;
        public LeftAdapter(Fragment fragment){
            this.fragment = fragment;
            this.list = new ArrayList<String>();
        }
        public void add(@NonNull String[] strings){
            for(String string: strings){
                this.list.add(string);
            }
            this.notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Fragment fragment = this.fragment;
        View itemView = this.fragment.getLayoutInflater().inflate(R.layout.item_left, null);
        TextView tv = itemView.findViewById(R.id.tvValue);
        tv.setText(this.list.get(position));
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("index", getItem(position).toString());
                String isi = tv.getText().toString();
                if(isi.equals(Static.LOGIN)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.LOGIN_FRAGMENT);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }
                else if(isi.equals(Static.MAIN)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.MAIN_FRAGMENT);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }
                else if(isi.equals(Static.RUTE)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.RUTE_FRAGMENT);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }
                else if(isi.equals(Static.SHUTTLE)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.PESAN_FRAGMENT);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }
                else if(isi.equals(Static.HISTORY)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.HISTORY_FRAGMENT);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }
                else if(isi.equals(Static.EXIT)){
                    Bundle result = new Bundle();
                    result.putInt(Static.PAGE, Static.EXIT_CLOSE);
                    fragment.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
                }


            }
        });
        return itemView;
    }


}
