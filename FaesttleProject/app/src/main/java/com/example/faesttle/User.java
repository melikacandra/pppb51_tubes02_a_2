package com.example.faesttle;

public class User {
    private String username;
    private String password;
    private String key;

    public User(String username, String password, String key){
        this.username = username;
        this.password = password;
        this.key = key;
    }
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getKey() {
        return key;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
