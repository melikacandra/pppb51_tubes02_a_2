package com.example.faesttle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class LeftFragment extends Fragment {
    protected LeftAdapter adapter;
    protected ListView lv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_left, container, false);
        String[] menu = {Static.LOGIN, Static.MAIN, Static.RUTE, Static.SHUTTLE, Static.HISTORY, Static.EXIT};
        this.adapter = new LeftAdapter(this);
        this.adapter.add(menu);
        this.lv = view.findViewById(R.id.lv_menu);
        this.lv.setAdapter(this.adapter);
        return view;
    }
}
