package com.example.faesttle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.faesttle.databinding.FragmentMainBinding;

public class MainFragment extends Fragment implements View.OnClickListener{
    private FragmentMainBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    public static MainFragment newInstance(){
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentMainBinding.inflate(inflater, container, false);

        this.binding.btnPesan.setOnClickListener(this);
        this.binding.btnRute.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if(v == binding.btnPesan){
            Bundle result = new Bundle();
            result.putInt(Static.PAGE, Static.PESAN_FRAGMENT);
            this.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
        }
        else if(v == binding.btnRute){
            Bundle result = new Bundle();
            result.putInt(Static.PAGE, Static.RUTE_FRAGMENT);
            this.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
        }
    }
}
