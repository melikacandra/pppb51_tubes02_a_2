package com.example.faesttle;

import java.util.List;

public class LoginPresenter {
    protected User user;
    protected ILogin ui;


    public void getUserData(String username, String password, String token){
        User user = new User(username, password, token);
    }
    public interface ILogin{
        public void login(String username, String password);
        public void changePage();
        public void textKosong();
        public void loginGagal();
        public void loginBerhasil();
        public void verify(String username, String password);
    }

}