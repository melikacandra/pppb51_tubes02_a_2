package com.example.faesttle;

public class Rute {
    private String source;
    private String destination;
    private int fee;

    public Rute(String source, String destination, int fee){
        this.source = source;
        this.destination = destination;
        this.fee = fee;
    }

    public int getFee() {
        return fee;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
