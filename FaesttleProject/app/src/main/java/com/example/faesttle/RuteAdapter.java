package com.example.faesttle;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.faesttle.databinding.ItemRuteBinding;

import java.util.ArrayList;
import java.util.List;

public class RuteAdapter extends BaseAdapter {
    private List<Rute> list;
    private Fragment fragment;

    public RuteAdapter(Fragment fragment) {
        this.fragment = fragment;
        this.list = new ArrayList<Rute>();
    }

    public void add(List<Rute> rutes) {
        for (Rute rute : rutes) {
            this.list.add(rute);
        }
        this.notifyDataSetChanged();
    }

    public void addLine(Rute rute) {
        this.list.add(rute);
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            ItemRuteBinding binding = ItemRuteBinding.inflate(this.fragment.getLayoutInflater(), viewGroup, false);
            view = binding.getRoot();
            viewHolder = new ViewHolder(binding);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.updateView(this.getItem(position), position);

        return view;
    }

    private class ViewHolder {
        private ItemRuteBinding binding;

        public ViewHolder(ItemRuteBinding binding) {
            this.binding = binding;
        }

        public void updateView(Object rute, int i) {
            Rute currentRute = (Rute) rute;
            this.binding.tvDestination.setText(currentRute.getDestination());
            this.binding.tvSource.setText(currentRute.getSource());
            this.binding.tvHarga.setText(Integer.toString(currentRute.getFee()));

        }
    }
}


