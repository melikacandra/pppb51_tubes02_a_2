package com.example.faesttle;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.faesttle.databinding.FragmentRuteBinding;
import com.example.faesttle.databinding.FragmentShuttleBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuteFragment extends Fragment {
    private FragmentRuteBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    private ProgressDialog loading;
    private RuteAdapter adapter;
    private List<Rute> rutes;
    private JSONArray ruteJson;
    public static RuteFragment newInstance(){
        RuteFragment fragment = new RuteFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.rutes = new ArrayList<Rute>();
        this.binding = FragmentRuteBinding.inflate(inflater, container, false);
        this.adapter = new RuteAdapter(this);
        this.adapter.add(this.rutes);
        this.binding.lvRute.setAdapter(this.adapter);
        loadData();

        return this.binding.getRoot();
    }

    private void loadData(){
        //loading = ProgressDialog.show(getContext(), "Memuat Data", "Harap Tunggu");
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://devel.loconode.com/pppb/v1/routes";
        JSONObject jsonObject = new JSONObject();

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response.toString());
                    Log.d("hasil", jo.toString());
                    JSONArray payload = jo.getJSONArray("payload");
                    ruteJson = payload;
                    Log.d("hasil", payload.toString());
                    JSONObject rute1 = payload.getJSONObject(0);

                    binding.tvSource.setText(rute1.getString("source").toString());
                    binding.tvDestination.setText(rute1.getString("destination").toString());
                    binding.tvHarga.setText(rute1.getString("fee").toString());

                    for(int i = 0; i<payload.length(); i++){
                        JSONObject rute = payload.getJSONObject(i);
                        Log.d("hasil", rute.toString());
                        String source = rute.getString("source");
                        String destination = rute.getString("rute");
                        int fee = rute.getInt("fee");
                        Rute currentrute = new Rute(source, destination, fee);
                        //adapter.addLine(rute1);
                       //rutes.add(currentrute);
                    }


                    Toast.makeText(getContext(), "Berhasil Memuat", Toast.LENGTH_SHORT).show();
                    //loading.cancel();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.cancel();
                Toast.makeText(getContext(), "Gagal ambil Rest API" + error, Toast.LENGTH_SHORT).show();
                Log.d("error", error.toString());
            }
        }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJfaWQiOiJlNGQ1YzI2Ni01MzVjLTExZWMtOTg2MS05ZmVmMGIyM2E3ZTkiLCJ1c2VybmFtZSI6IjYxODE5MDEwNjUifSwiaWF0IjoxNjM5NjQzNDEyfQ.JcEzJFv5C3WqHY5JeB0z2jiSPYO3NfHURgdwad0qzy8");
                return params;
            }
        };
        queue.add(stringRequest);

    }

}
