package com.example.faesttle;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.faesttle.databinding.FragmentLoginBinding;
import com.example.faesttle.databinding.FragmentMainBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends Fragment implements View.OnClickListener, LoginPresenter.ILogin {
    private FragmentLoginBinding binding;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    public LoginPresenter presenter;
    public ProgressDialog loading;
    public static LoginFragment newInstance(){
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentLoginBinding.inflate(inflater, container, false);
        this.binding.btnLogin.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if(v == binding.btnLogin){
            login(binding.etUsername.getText().toString(), binding.etPassword.getText().toString());
        }
    }

    @Override
    public void login(String username, String password) {
        if(username.equals("") || password.equals("")){
            textKosong();
        }
        else{
            verify(username, password);

        }

    }

    @Override
    public void changePage() {
        Bundle result = new Bundle();
        result.putInt(Static.PAGE, Static.MAIN_FRAGMENT);
        this.getParentFragmentManager().setFragmentResult(Static.CHANGE_PAGE, result);
    }

    @Override
    public void textKosong() {
        Toast.makeText(getContext(),"username dan password tidak boleh kosong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginGagal() {
        Toast.makeText(getContext(),"username atau password salah",  Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginBerhasil() {
        Toast.makeText(getContext(),"login berhasil",  Toast.LENGTH_SHORT).show();
    }

    @Override
    public void verify(String username, String password) {
        loading = ProgressDialog.show(getContext(), "Sedang Masuk", "Harap Tunggu");
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://devel.loconode.com/pppb/v1/authenticate";
        JSONObject jsonObject = new JSONObject();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response.toString());
                    Log.d("hasil", jo.toString());
                    String token = jo.getString("token");
                    String message = jo.getString("message");
                    if(message.equals("Authentication successful")){
                        //presenter.getUserData(username, password, token);
                        loading.cancel();
                        loginBerhasil();
                        changePage();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.cancel();
                loginGagal();
                Log.d("error", error.toString());
            }
        }
        ){
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJfaWQiOiJlNGQ1YzI2Ni01MzVjLTExZWMtOTg2MS05ZmVmMGIyM2E3ZTkiLCJ1c2VybmFtZSI6IjYxODE5MDEwNjUifSwiaWF0IjoxNjM5NjQzNDEyfQ.JcEzJFv5C3WqHY5JeB0z2jiSPYO3NfHURgdwad0qzy8");
//                return params;
//            }
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        queue.add(stringRequest);
    }
}
